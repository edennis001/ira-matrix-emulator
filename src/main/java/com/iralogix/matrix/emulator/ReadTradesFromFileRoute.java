package com.iralogix.matrix.emulator;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.fixed.BindyFixedLengthDataFormat;

import java.util.List;

public class ReadTradesFromFileRoute extends RouteBuilder {


    /**
     * The 80 File or the RO File is a file published by <b>BroadRidge</b> that contains a list of funds with the cusip number,
     * and what type of communication needs to go out to the holder.
     * <p>
     * This route reads this file from the ftp url and converts to POJO which can be used by rest of application.
     */

    @Override
    public void configure() {
        from("file:/Users/edennis/Desktop?fileName=TRADE20191022964.txt")
            // save file locally before processing
            .unmarshal(new BindyFixedLengthDataFormat(TradeOrderInstructions.class)).process(exchange -> {
            // convert to class
            List<TradeOrderInstructions> results = exchange.getIn().getBody(List.class);
            exchange.getIn().setBody("WOW");
            System.out.println("read file");
        }).to("file:/Users/edennis/Desktop?fileName=OUT.txt");

    }


}
