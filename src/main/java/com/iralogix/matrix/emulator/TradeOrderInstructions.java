package com.iralogix.matrix.emulator;

import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@Data
@FixedLengthRecord(length = 80, ignoreTrailingChars = true)
public class TradeOrderInstructions {

    static final long serialVersionUID = 1L;

    @DataField(pos = 1, length = 6)
    private String fileId;

    @DataField(pos = 7, length = 1)
    private String alternateJobType;

    @DataField(pos = 8, length = 5)
    private String pastRecordDate;

    @DataField(pos = 21, length = 30)
    private String registrantSecurityDescription;

    @DataField(pos = 51, length = 6)
    private String recordDate;

    @DataField(pos = 63, length = 9)
    private String cusipNumber;

    @DataField(pos = 78, length = 1)
    private String typeOfMailing;


}
