package com.iralogix.matrix.emulator;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class Application {

    public static void main(String args[]) throws Exception {
        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new ReadTradesFromFileRoute());

        context.start(); // start the route
         Thread.sleep(3000000L); // let the route run for 30 seconds
         context.stop(); // stop the route
    }
}
